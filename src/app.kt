fun findLowestDivisable(n: Int) {
    var answer = 0
    var loop = true

    while (loop) {
        answer ++
        for (i in 1..n) {
            if (answer % i != 0) break
            else if (i == n) loop = false
        }
    }
    println(answer)
}